package convert_test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import main.FileProcessor;

import org.junit.Test;

import common.Comment;
import common.SharedObjects;
import common.Word;
import convert.Convertor;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap; 
import java.util.Map;
import lv.ailab.lnb.fraktur.translit.ResultData;
import methods.TranslitProcessor;

public class ConvertorTests {

	/*@Test
	public void test() {
		String input="C:\\Users\\Ginta\\Documents\\GitHub\\TrainingDataNaiveBayes\\barometrs\\neagresivi_10k.txt";
		String output="C:\\Users\\Ginta\\Documents\\GitHub\\TrainingDataNaiveBayes\\barometrs\\neagresivi_10k.txt.conv";
		try {
			Convertor r = SharedObjects.getConvertor();
			ArrayList<Comment> comments=r.convertComments(input);
			//FileProcessor.printCommentsToTxtFile(comments, output);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
        
        @Test
        public void test1() {
            String input="C:\\Users\\Ginta\\Documents\\GitHub\\TrainingDataNaiveBayes\\barometrs\\agresivi_10k"; //args[0];
            String output="C:\\Users\\Ginta\\Documents\\GitHub\\TrainingDataNaiveBayes\\barometrs\\agresivi_10k.conv"; //args[1];
        
            try 
            {
                Convertor r = SharedObjects.getConvertor();
                BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(input), "utf-8"));
                String line,result,word;
                Comment comment,tmp;
                TranslitProcessor processor=SharedObjects.getTranslitProcessor();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output),"UTF-8"));
                int counter[]=new int[3];
                int i;
                int c=0;
                for(i=0;i<3;i++) counter[i]=0;
		while ((line = br.readLine()) != null) 
                {
                    comment=new Comment(line);
                    //processor.analyzeWordList(comment);
                    //processor.calculateCumulativeResult(comment);
                    tmp = new Comment(null);
                    
                    for(i=0;i<3;i++) counter[i]++;
                    if(counter[1]==100)
                    {
                        System.out.print('.');
                        counter[1]=0;
                    }
                    if(counter[2]==1000)
                    {
                        System.out.println();
                        counter[2]=0;
                        bw.flush();
                    }
                    if(counter[2]==10000)
                    {
                        System.out.println("-------------------------------");
                        counter[2]=0;
                    }
                    
                    c++;
                    word=line.split("\t")[1];
                    if(!word.matches(".*[a-zēūīāšģķļžčņ].*")) continue;
                    ResultData data=r.getResults(word);
                                if(!(data.DICT_EXACT.data.size()!=0 || data.DICT_FUZZY.data.size()!=0 || data.DICT_EXACT_GUESS.data.size()!=0 || data.DICT_FUZZY_GUESS.data.size()!=0))
                                {
                                    
                    bw.write(line);
                    bw.write('\n');
                                }
                    //System.out.println(c);
                    
                    
		}
                bw.close();
		br.close();
            } 
            catch (Exception e) 
            {
                    e.printStackTrace();
            }
        }


    /*@Test
    public void test2() {
            
        File trainingDir = new File("C:\\Users\\Ginta\\Documents\\GitHub\\naivebayesclassifier\\resources\\datasets\\trainingConvOne");
        File trainingCategoriesDirs[] = trainingDir.listFiles( new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return new File(dir, name).isDirectory();
                }
            }
        );
                
        for( File trainingCategoryDir : trainingCategoriesDirs ) {
            File trainingFiles[] = trainingCategoryDir.listFiles();
            for( File trainingFile : trainingFiles ) {
		String input=trainingFile.getAbsolutePath();
		String output=(new File(trainingFile.getParent(),trainingFile.getName()+".conv")).getAbsolutePath();
		try {
			Convertor r = SharedObjects.getConvertor();
			ArrayList<Comment> comments=r.convertComments(input);
			FileProcessor.printCommentsToTxtFile(comments, output);
		} catch (Exception e) {
			e.printStackTrace();
		}
            }
        }

    }*/

}
