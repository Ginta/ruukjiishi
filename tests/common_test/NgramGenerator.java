/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package common_test;

import common.SharedObjects;
import common.Word;
import static java.awt.PageAttributes.MediaType.C;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import methods.TranslitProcessor;
import org.junit.Test;
import splitter.Splitting;

/**
 *
 * @author Ginta
 */
public class NgramGenerator {
    
    @Test
    public void generate() throws Exception
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\Users\\Ginta\\Documents\\GitHub\\LVTagger\\balanseetais_text.txt"), "utf-8"));
        
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("C:\\Users\\Ginta\\Documents\\GitHub\\LVTagger\\balanseetais_tokenized.txt"), "utf-8"));
        
    
        String line;
        while ((line = br.readLine()) != null) {
            ArrayList<Word> words=Splitting.tokenize(line);
            
            for (Word a : words)
            {
                if(a.isWord) {
                    out.write(a.value);
                    out.write(' ');
                }
                //out.write(a.value);
                if(a.value.equals("?") || a.value.equals(".")|| a.value.equals("!"))
                    out.write('\n');
            }
             out.write('\n');
	}
	br.close();
        out.close();
    } 
}
