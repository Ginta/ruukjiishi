/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package preprocess;

import common.SharedObjects;
import com.cybozu.labs.langdetect.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import rsu.Main;

/**
 *
 * @author Ojars
 */
public class LanguageDetector {
    
    private static final Logger log = Logger.getLogger(LanguageDetector.class.getName());
    
    public static void langdetect() {
        Connection connInp, connOut;
        Statement stmt;
        ResultSet rs;
        try {
            DetectorFactory.loadProfile("profiles");
            
            connInp = Main.getDbConnection();
            connOut = Main.getDbConnection();
            
            PreparedStatement insOutLang = connOut.prepareStatement("INSERT INTO comment_lang(id,lang) VALUES (?,?)");
                    
            //stmt = connInp.createStatement();
            stmt = connInp.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
            stmt.setFetchSize(Integer.MIN_VALUE);
            //rs = stmt.executeQuery("SELECT id,text FROM comment LIMIT "+limit+" OFFSET "+offset);
            rs = stmt.executeQuery("SELECT id,text FROM comment");
            log.info("Selected..");
            int iRow=0;
            while (rs.next()) {
                String line=rs.getString("text");
                
                Detector detector = DetectorFactory.create();
                //detector.setAlpha(0.5);
                //detector.setVerbose();
                detector.append(line.toLowerCase());
                String lang;
                try {
                    lang = detector.detect();
                } catch (LangDetectException ex) {
                    //Logger.getLogger(LanguageDetector.class.getName()).log(Level.SEVERE, null, ex);
                    //log.info("Undetected lagnguage: "+line);
                    lang="--";
                }
                    
                insOutLang.setInt(1, rs.getInt("id"));
                insOutLang.setString(2, lang );
                insOutLang.executeUpdate();
                /*
                if( !lang.equals("lv") ) {
                    log.info(lang+": "+line);
                }
                */
                if((++iRow % 1000)==0 ) {
                    log.info("Rows processed: "+iRow);
                }
            }
            log.info("Rows processed: "+iRow+" - all.");
            connInp.close();
            connOut.close();
        } catch (SQLException ex) {
            Logger.getLogger(LanguageDetector.class.getName()).log(Level.SEVERE, null, ex);
        } catch (LangDetectException ex) {
            Logger.getLogger(LanguageDetector.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }
    
}
