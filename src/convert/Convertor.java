package convert;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import common.Comment;
import common.Word;

import lv.ailab.lnb.fraktur.Transliterator;
import lv.ailab.lnb.fraktur.translit.Engine;
import lv.ailab.lnb.fraktur.translit.ResultData;
import lv.ailab.lnb.fraktur.translit.Rules;
import lv.ailab.lnb.fraktur.translit.Variant;
import lv.semti.morphology.analyzer.Analyzer;
import main.FileProcessor;
import common.LRUCache;
import methods.Ngrams;

public class Convertor {
	Rules rules;
	Transliterator t;
        LRUCache<String, String> cache;
        Ngrams word_ngrams;
	
	public Convertor() throws Exception
	{
		Analyzer analyzer=new Analyzer();
		analyzer.enableGuessing=true;
                cache=new LRUCache<String, String>(1000) {};
		t = Transliterator.getTransliterator(analyzer);
                word_ngrams=new Ngrams("<s>", "</s>", "<unk>", "models/ngramModels/balanseetais_plain.vocab", "models/ngramModels/balanseetais_plain.model", 3);
	}
	
	
	public ResultData getResults(String token)
	{
		
		try {
			return t.processWord(token, "core", true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String getBestMatch(String word)
	{
		String result=cache.get(word);
                if(result==null)
                {
                    ArrayList<Variant> sorted = new ArrayList<Variant>();

                    ResultData r=this.getResults(word);

                    sorted.addAll(r.DICT_EXACT.data.keySet());
                    sorted.addAll(r.DICT_EXACT_GUESS.data.keySet());

                    if(sorted.isEmpty())
                    {
                            sorted.addAll(r.DICT_FUZZY.data.keySet());
                            sorted.addAll(r.DICT_FUZZY_GUESS.data.keySet());
                    }

                    if(sorted.isEmpty())
                    {
                            sorted.addAll(r.NO_DICT_EXACT);
                    }

                    if(sorted.isEmpty())
                    {
                            sorted.addAll(r.NO_DICT_FUZZY);
                    }

                    if(sorted.isEmpty())
                    {
                            result=word;
                    }
                    else
                    {
                            Collections.sort(sorted, t.comparator);
                            result=sorted.get(0).token;
                    }
                    
                    cache.put(word,result);
                }
		
		return result;
	}
        
        public ArrayList<Variant> getBestMatches(String word)
	{
                ArrayList<Variant> sorted = new ArrayList<Variant>();

                ResultData r=this.getResults(word);

                sorted.addAll(r.DICT_EXACT.data.keySet());
                sorted.addAll(r.DICT_EXACT_GUESS.data.keySet());

                sorted.addAll(r.DICT_FUZZY.data.keySet());
                sorted.addAll(r.DICT_FUZZY_GUESS.data.keySet());

                if(sorted.isEmpty())
                {
                        sorted.addAll(r.NO_DICT_EXACT);
                }

                if(sorted.isEmpty())
                {
                        sorted.addAll(r.NO_DICT_FUZZY);
                }
                
                if(sorted.isEmpty())
                {
                    sorted.add(new Variant(word, null));
                }
		
		return sorted;
	}
        
        private int[] getBestSequence(ArrayList<ArrayList<Variant>> variants)
        {
            int counters[]=new int[variants.size()];
            int best_match[]=new int[variants.size()];
            ArrayList<String> sentence;
            
            double best_prob=0,prob;
            int i;
            
            for(i=0;i<counters.length;i++) counters[i]=0;
            
            while(true)
            {
                sentence=new ArrayList<String>(variants.size());
                
                for(i=0;i<counters.length;i++)
                {
                    sentence.add(variants.get(i).get(counters[i]).token);
                }
                
                prob=word_ngrams.evaluateToken(sentence);
                if(prob>best_prob || best_prob==0)
                {
                    best_prob=prob;
                    for(i=0;i<counters.length;i++)
                    {
                        best_match[i]=counters[i];
                    }
                }
                
                counters[0]++;
                i=0;
                while(i<counters.length-1 && counters[i]>=variants.get(i).size())
                {
                    counters[i+1]++;
                    counters[i]=0;
                    i++;
                }
                
                if(counters[counters.length-1] == variants.get(counters.length-1).size())
                {
                    break;
                }
            }
            
            return best_match;
        }
        
        public Comment convertComment(Comment comment)
        {
            Comment c=new Comment(null);
            ArrayList<ArrayList<Variant>> variants=new ArrayList<ArrayList<Variant>>();
            ArrayList<Variant> tmp;
            
            for(Word word : comment.words)
            {
                if(word.isWord)
                {
                    tmp=getBestMatches(word.value);
                    variants.add(tmp);
                    //System.out.print(tmp.size());
                    //System.out.println("\t"+tmp.toString());
                }
            }
            
            int window=3;
            int best_match[]=new int[variants.size()],i,j,ls1=0,l1=-1,c1,matched_window[];
            ArrayList<ArrayList<Variant>> sublist;
            
            for(i=0;i<variants.size();i++)
            {
                if(variants.get(i).size()==1)
                {
                    if(l1==-1) l1=i;
                }
                else
                {
                    if(i-l1>=window-1 && l1!=-1)
                    {
                        sublist=new ArrayList<ArrayList<Variant>>(l1-ls1+1);
                        for(j=ls1;j<l1+2;j++)
                        {
                            sublist.add(variants.get(j));
                        }
                        matched_window=getBestSequence(sublist);
                        
                        for(j=ls1;j<l1+2;j++)
                        {
                            best_match[j]=matched_window[j-ls1];
                        }
                        
                        ls1=i-2;
                    }
                    l1=-1;
                }
            }
            
            l1=variants.size()-1;
           sublist=new ArrayList<ArrayList<Variant>>(variants.size()-ls1+1);
            for(j=ls1;j<l1+1;j++)
            {
                sublist.add(variants.get(j));
            }
            matched_window=getBestSequence(sublist);

            for(j=ls1;j<l1+1;j++)
            {
                best_match[j]=matched_window[j-ls1];
            }
            
            i=0;
            for(Word word : comment.words)
            {
                if(word.isWord)
                {
                    c.words.add(new Word(variants.get(i).get(best_match[i]).token));
                    i++;
                }
                else
                {
                    c.words.add(word);
                }
            }
            
            return c;
        }

	public ArrayList<Comment> convertComments(String fileName) throws Exception
	{
		ArrayList<Comment> comments = FileProcessor.processFile(fileName);
		ArrayList<Comment> resultComments=new ArrayList<Comment>();
		String result;
		
		
		for(Comment comment : comments)
		{
			System.out.println();
			
			Comment tmp = new Comment(null);
			
			if (true /*comment.cumulativeProbabilty>=0.5*/)
			{
				for(Word word : comment.words)
				{
					if(word.ngramTranslitProbability>0.0)
					{
						if(word.isWord)
						{
							result=getBestMatch(word.value);
						}
						else
						{
							result=word.value;
						}
						
						tmp.words.add(new Word(result));		
						System.out.print(result);
					}
					
					else
					{
						tmp.words.add(word);		
						System.out.print(word);
					}
				}
				resultComments.add(tmp);
			}
			
			else
			{
				resultComments.add(comment);
			}				
		}
		
		return resultComments;		
	}
}
